﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Intbus.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intbus.Extensions.Tests
{
    [TestClass()]
    public class ByteArrayExtensionTests
    {
        [TestMethod()]
        public void ReverseByWordTest()
        {
            byte[] bytes = new byte[] { 0x01, 0x02, 0x03, 0x04 };

            var actual = bytes.ReverseByByte();
            CollectionAssert.AreEqual(new byte[] { 0x02, 0x01, 0x04, 0x03 }, actual.ToArray());

            actual = bytes.ReverseByByte().ReverseByWord();
            CollectionAssert.AreEqual(new byte[] { 0x04, 0x03, 0x02, 0x01 }, actual.ToArray());
        }
    }
}