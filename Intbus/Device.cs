﻿using Intbus.Exceptions;
using Intbus.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Intbus
{
    public class Device
    {
        private static int bufferSize = 512;
        public Device(Stream stream, Interface? _interface, byte address, string name)
        {
            this.Interface = _interface;
            this.Address = address;
            this.Stream = stream;
            this.Name = name;
        }

        public Device(Stream stream, Interface? _interface, byte address, List<Device> slaveDevices, string name)
            : this(stream, _interface, address, name)
        {
            this.devices = slaveDevices;
            devices?.ForEach(d =>
            {
                d.Parent = this;
                d.IncreaseDepth();
            });
        }

        public void AddSlave(Device device)
        {
            if (this.devices == null)
                this.devices = new List<Device>();
            device.Parent = this;
            device.IncreaseDepth();
            this.devices.Add(device);
        }
        private int address;
        private Interface GetInteface(byte fullAddress) => (Interface)(fullAddress >> 5);
        private byte GetAddress(byte fullAddress) => (byte)(fullAddress & 0x1F);
        private List<Device> devices;

        private static Dictionary<Interface?, byte> interfaceCodeDictionary = new Dictionary<Interface?, byte>
        {
            {Intbus.Interface.UART, 0b001 },
            {Intbus.Interface.FM,   0b010 },
            {Intbus.Interface.SPI,  0b011 },
            {Intbus.Interface.I2C,  0b100 },
            {Intbus.Interface.OWI,  0b101 }
        };

        public Stream Stream { get; set; }
        private void IncreaseDepth()
        {
            this.Depth++;
            this.devices?.ForEach(d => d.IncreaseDepth());
        }
        public int Address
        {
            get => address;
            private set
            {
                if (value < 1 || value > 255)
                    throw new DeviceException(this, $"Intbus адрес должен быть от 1 до 255 [{address}]");
                address = value;
            }
        }
        public byte[] ExtendedAddress =>
            Address < 31 ? new byte[] { (byte)Address } : new byte[] { 31, (byte)(Address) };
        public Interface? Interface { get; private set; }

        public delegate void BytesHandler(IEnumerable<byte> bytes);
        public event BytesHandler BytesReceived;
        public event BytesHandler BytesSended;
        public string Name { get; set; }
        public int Depth { get; private set; }
        public IEnumerable<Device> Devices => devices;
        public Device Parent { get; set; }

        public Device RootDevice => Parent != null ? Parent.RootDevice : this;
        public int StreamTimeout { get; set; } = 100;

        public int WakeUpByteAmount { get; set; } = 0;
        public int ResponseTimeout { get; set; } = 1000;
        public int RequestTimeout { get; set; } = 1000;
        public IEnumerable<byte> GetFullPreambule()
        {
            List<byte> bytes = new List<byte>();
            Device dev = this;
            bytes.AddRange(Enumerable.Range(0, dev.WakeUpByteAmount).Select(b => (byte)0xFF));
            bytes.AddRange(dev.ExtendedAddress);
            while (dev.Parent != null)
            {
                byte[] extendedAddress = dev.Parent.ExtendedAddress;
                extendedAddress[0] |= (byte)(interfaceCodeDictionary[dev.Interface] << 5);
                bytes.InsertRange(0, extendedAddress);
                bytes.InsertRange(0, Enumerable.Range(0, dev.Parent.WakeUpByteAmount).Select(b => (byte)0xFF));
                dev = dev.Parent;
            }
            //bytes.InsertRange(0, this.ExtendedAddress);

            return bytes;
        }

        public async Task<Response> ReadHoldingRegsAsync(UInt16 firstReg, UInt16 amount)
        {
            return await ReadRegsAsync(firstReg, amount, 0x03);
        }

        private async Task<Response> ReadRegsAsync(ushort firstReg, ushort amount, byte functionNum)
        {
            IEnumerable<byte> request = this.GetFullPreambule();
            int baseWakeUpBytes = request.Count() - request.SkipWhile(b => b == 0xFF).Count();
            int preambuleLength = request.Count() - baseWakeUpBytes;

            request = request.Concat(new byte[]
                { functionNum, (byte)(firstReg >> 8), (byte)firstReg, (byte)(amount >> 8), (byte)amount });


            IEnumerable<byte> crc = CRC.CalculateCrc(request.Skip(baseWakeUpBytes));
            request = request.Concat(crc);
            //Байты для пробуждения о-Модем
            request = Enumerable.Range(0, this.WakeUpByteAmount).Select(x => (byte)0xff).Concat(request);

            DateTime beginTime = DateTime.Now;
            if (!this.Stream.WriteAsync(request.ToArray(), 0, request.Count()).Wait(this.RequestTimeout))
                throw new DeviceException(RootDevice, "Время ожидания отправления запроса истекло");

            this.BytesSended?.Invoke(request);

            byte[] buffer = new byte[bufferSize];

            int expectedLength = preambuleLength + 1 + 1 + (amount * 2) + 2;
            int actualLength = 0;
            this.Stream.ReadTimeout = this.ResponseTimeout;
            try
            {
                actualLength = this.Stream.Read(buffer, 0, buffer.Length);
            }
            catch (Exception)
            {
                throw new DeviceException(RootDevice, "Время ожидания ответа истекло");
            }

            if (actualLength > expectedLength)
                return new Response(this, buffer.Take(actualLength), beginTime, Error.ResponseOverflow);

            if (!CRC.IsValidCrc(buffer.Take(actualLength)))
            {
                // На ПК могут рваться посылки, поэтому после первого чтения ждем и еще раз пытаемся дочитать.
                await Task.Delay(this.StreamTimeout);
                try
                {
                    actualLength += this.Stream.Read(buffer, actualLength, buffer.Length - actualLength);
                    this.BytesReceived?.Invoke(buffer.Take(actualLength));
                    if (actualLength < 3)
                        return new Response(this, buffer.Take(actualLength), beginTime, Error.ShortResponse);
                }
                catch (Exception)
                {
                    throw new DeviceException(RootDevice, "Время ожидания ответа истекло");
                }

                if (!CRC.IsValidCrc(buffer.Take(actualLength)))
                    return new Response(this, buffer.Take(actualLength), beginTime, Error.CRC);
            }
            else
            {
                this.BytesReceived?.Invoke(buffer.Take(actualLength));
            }

            // Если длина ответа отличается от ожидаемой, ищем ошибку
            if (actualLength < expectedLength)
            {
                var responseBuf = buffer.Take(actualLength - 2).ToArray();
                var requestBuf = request.Skip(baseWakeUpBytes).ToArray();
                return this.ParseError(responseBuf, requestBuf, preambuleLength, functionNum, beginTime);
            }

            int dataLength = buffer.Skip(preambuleLength + 1).First();

            return new Response(this, buffer.Skip(preambuleLength + 2).Take(dataLength), beginTime);
        }

        public async Task<Response> ReadInputRegsAsync(UInt16 firstReg, UInt16 amount)
        {
            return await ReadRegsAsync(firstReg, amount, 0x04);
        }

        private async Task<Response> WriteRegAsync(ushort address, UInt16 value, byte functionNum)
        {
            IEnumerable<byte> request = this.GetFullPreambule();
            int baseWakeUpBytes = request.Count() - request.SkipWhile(b => b == 0xFF).Count();
            int preambuleLength = request.Count() - baseWakeUpBytes;

            request = request.Concat(new byte[]
                { functionNum, (byte)(address >> 8), (byte)address, (byte)(value >> 8), (byte)value });

            IEnumerable<byte> crc = CRC.CalculateCrc(request.Skip(baseWakeUpBytes));
            request = request.Concat(crc);

            DateTime beginTime = DateTime.Now;
            if (!this.Stream.WriteAsync(request.ToArray(), 0, request.Count()).Wait(this.RequestTimeout))
                throw new DeviceException(RootDevice, "Время ожидания отправления запроса истекло");

            this.BytesSended?.Invoke(request);

            byte[] buffer = new byte[bufferSize];

            int expectedLength = preambuleLength + 1 + 2 + 2 + 2;
            int actualLength = 0;
            this.Stream.ReadTimeout = this.ResponseTimeout;
            try
            {
                actualLength = this.Stream.Read(buffer, 0, buffer.Length);
            }
            catch (Exception)
            {
                throw new DeviceException(RootDevice, "Время ожидания ответа истекло");
            }

            if (actualLength > expectedLength)
                return new Response(this, buffer.Take(actualLength), beginTime, Error.ResponseOverflow);

            if (!CRC.IsValidCrc(buffer.Take(actualLength)))
            {
                // На ПК могут рваться посылки, поэтому после первого чтения ждем и еще раз пытаемся дочитать.
                await Task.Delay(this.StreamTimeout);
                try
                {
                    actualLength += this.Stream.Read(buffer, actualLength, buffer.Length - actualLength);
                    this.BytesReceived?.Invoke(buffer.Take(actualLength));
                    if (actualLength < 3)
                        return new Response(this, buffer.Take(actualLength), beginTime, Error.ShortResponse);
                }
                catch (Exception)
                {
                    throw new DeviceException(RootDevice, "Время ожидания ответа истекло");
                }

                if (!CRC.IsValidCrc(buffer.Take(actualLength)))
                    return new Response(this, buffer.Take(actualLength), beginTime, Error.CRC);
            }
            else
            {
                this.BytesReceived?.Invoke(buffer.Take(actualLength));
            }

            // Если длина ответа отличается от ожидаемой, ищем ошибку
            if (actualLength < expectedLength)
            {
                var responseBuf = buffer.Take(actualLength - 2).ToArray();
                var requestBuf = request.Skip(baseWakeUpBytes).ToArray();
                return this.ParseError(responseBuf, requestBuf, preambuleLength, functionNum, beginTime);
            }

            int dataLength = buffer.Skip(preambuleLength + 1).First();

            return new Response(this, buffer.Skip(preambuleLength + 2).Take(dataLength), beginTime);
        }

        public async Task<Response> WriteHoldingMultiplyAsync(UInt16 address, IEnumerable<UInt16> values)
        {
            return await this.WriteRegsAsync(address, values, 0x10);
        }
        private async Task<Response> WriteRegsAsync(ushort address, IEnumerable<UInt16> values, byte functionNum)
        {
            IEnumerable<byte> request = this.GetFullPreambule();
            int baseWakeUpBytes = request.Count() - request.SkipWhile(b => b == 0xFF).Count();
            int preambuleLength = request.Count() - baseWakeUpBytes;

            request = request.Concat(new byte[] { functionNum, (byte)(address >> 8), (byte)address });
            int count = values.Count();
            request = request.Concat(new byte[] { (byte)((count) >> 8), (byte)(count), (byte)(values.Count() * 2) });
            foreach (var value in values)
            {
                request = request.Concat(new byte[] { (byte)(value >> 8), (byte)value });
            }

            IEnumerable<byte> crc = CRC.CalculateCrc(request.Skip(baseWakeUpBytes));
            request = request.Concat(crc);

            DateTime beginTime = DateTime.Now;
            if (!this.Stream.WriteAsync(request.ToArray(), 0, request.Count()).Wait(this.RequestTimeout))
                throw new DeviceException(RootDevice, "Время ожидания отправления запроса истекло");

            this.BytesSended?.Invoke(request);

            byte[] buffer = new byte[bufferSize];

            int expectedLength = preambuleLength + 1 + 2 + 2 + 2;
            int actualLength = 0;
            this.Stream.ReadTimeout = this.ResponseTimeout;
            try
            {
                actualLength = this.Stream.Read(buffer, 0, buffer.Length);
            }
            catch (Exception)
            {
                throw new DeviceException(RootDevice, "Время ожидания ответа истекло");
            }

            if (actualLength > expectedLength)
                return new Response(this, buffer.Take(actualLength), beginTime, Error.ResponseOverflow);

            if (!CRC.IsValidCrc(buffer.Take(actualLength)))
            {
                // На ПК могут рваться посылки, поэтому после первого чтения ждем и еще раз пытаемся дочитать.
                await Task.Delay(this.StreamTimeout);
                try
                {
                    actualLength += this.Stream.Read(buffer, actualLength, buffer.Length - actualLength);
                    this.BytesReceived?.Invoke(buffer.Take(actualLength));
                    if (actualLength < 3)
                        return new Response(this, buffer.Take(actualLength), beginTime, Error.ShortResponse);
                }
                catch (Exception)
                {
                    throw new DeviceException(RootDevice, "Время ожидания ответа истекло");
                }

                if (!CRC.IsValidCrc(buffer.Take(actualLength)))
                    return new Response(this, buffer.Take(actualLength), beginTime, Error.CRC);
            }
            else
            {
                this.BytesReceived?.Invoke(buffer.Take(actualLength));
            }

            // Если длина ответа отличается от ожидаемой, ищем ошибку
            if (actualLength < expectedLength)
            {
                var responseBuf = buffer.Take(actualLength - 2).ToArray();
                var requestBuf = request.Skip(baseWakeUpBytes).ToArray();
                return this.ParseError(responseBuf, requestBuf, preambuleLength, functionNum, beginTime);
            }

            int dataLength = buffer.Skip(preambuleLength + 1).First();

            return new Response(this, buffer.Skip(preambuleLength + 2).Take(dataLength), beginTime);
        }
        public async Task<Response> WriteCoilRegsAsync(ushort address, bool newState)
        {
            return await WriteRegAsync(address, newState == true ? (UInt16)0xFF00 : (UInt16)0, 0x05);
        }

        public async Task<Response> WriteBuffer(IEnumerable<byte> request)
        {
            DateTime beginTime = DateTime.Now;
            if (!this.Stream.WriteAsync(request.ToArray(), 0, request.Count()).Wait(this.RequestTimeout))
                throw new DeviceException(RootDevice, "Время ожидания отправления запроса истекло");
            this.BytesSended?.Invoke(request);

            int actualLength;
            byte[] buffer = new byte[bufferSize];
            this.Stream.ReadTimeout = this.ResponseTimeout;
            try
            {
                actualLength = this.Stream.Read(buffer, 0, buffer.Length);
            }
            catch (Exception)
            {
                throw new DeviceException(RootDevice, "Время ожидания ответа истекло");
            }

            if (!CRC.IsValidCrc(buffer.Take(actualLength)))
            {
                // На ПК могут рваться посылки, поэтому после первого чтения ждем и еще раз пытаемся дочитать.
                await Task.Delay(this.StreamTimeout);
                try
                {
                    actualLength += this.Stream.Read(buffer, actualLength, buffer.Length - actualLength);
                    this.BytesReceived?.Invoke(buffer.Take(actualLength));
                    if (actualLength < 3)
                        return new Response(this, buffer.Take(actualLength), beginTime, Error.ShortResponse);
                }
                catch (Exception)
                {
                    throw new DeviceException(RootDevice, "Время ожидания ответа истекло");
                }

                if (!CRC.IsValidCrc(buffer.Take(actualLength)))
                    return new Response(this, buffer.Take(actualLength), beginTime, Error.CRC);
            }
            else
            {
                this.BytesReceived?.Invoke(buffer.Take(actualLength));
            }

            return new Response(this, buffer.Take(actualLength), beginTime);
        }
        public async Task<Response> WriteHoldingRegAsync(ushort address, UInt16 value)
        {
            return await WriteRegAsync(address, value, 0x06);
        }
        private Response ParseError(byte[] responseBuf, byte[] requestBuf, int preambuleLength, byte funcNum, DateTime beginTime)
        {
            for (int i = 0; i < responseBuf.Length; i++)
            {
                if (requestBuf[i] != responseBuf[i])
                {
                    Device dev = this;
                    while (dev != null)
                    {
                        if (dev.Depth == i)
                            break;
                        dev = dev.Parent;
                    }
                    
                    //A.5 A.6
                    if (GetAddress(responseBuf[i]) == 0
                        && responseBuf.Length == i + 2
                        && responseBuf[i + 1] == 0xFF)
                    {
                        return new Response(dev.Parent, responseBuf, beginTime, Error.IbInterfaceNotSupport);
                    }

                    //A.7 A.8 A.9
                    if (responseBuf[i] == 0xFF && responseBuf.Length == i + 2)
                    {
                        switch (responseBuf[i + 1])
                        {
                            case 0x05: return new Response(dev.Parent, responseBuf, beginTime, Error.IbCommandExecuting);
                            case 0x06: return new Response(dev.Parent, responseBuf, beginTime, Error.IbInterfaceBusy);
                            case 0x0A: return new Response(dev.Parent, responseBuf, beginTime, Error.IbTransmitError);
                            default: throw new DeviceException(this, "Передача информации занимает время, но данное состояние не идентифицировано"); ;
                        }
                    }

                    //A.3 A.4
                    if (GetAddress(responseBuf[i]) == 0
                        && responseBuf.Length == i + 2)
                    //&& GetAddress(requestBuf[preambuleLength - 1]) == GetAddress(responseBuf[i + 1]))
                    {
                        return new Response(dev, responseBuf, beginTime, Error.IbNotRespond);
                    }
                    break;
                }
                else if (this.GetInteface(responseBuf[i]) == Intbus.Interface.Self
                    && responseBuf.Length == i + 3)
                {
                    Device dev = this;
                    while (dev != null)
                    {
                        if (dev.Depth == i)
                            break;
                        dev = dev.Parent;
                    }
                    //A.2 Modbus Error
                    byte actualFunc = responseBuf[i + 1];
                    if ((actualFunc & 0x0F) != funcNum)
                        return new Response(dev, responseBuf, beginTime, Error.UnexpectedFunction);
                    if (actualFunc == (funcNum | 0x80))
                        return new Response(dev, responseBuf, beginTime, Response.ErrorMbCodeDictionary[responseBuf[i + 2]]);
                    break;
                }
            }

            throw new DeviceException(this, "Длина ответа отличается от ожидаемой, ответ с непредусмотренной ошибкой");

        }
    }

    public enum Interface
    {
        Self,
        UART,
        FM,
        I2C,
        SPI,
        OWI
    }
}
