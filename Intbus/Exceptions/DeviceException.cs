﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Intbus.Exceptions
{
    public class DeviceException : ApplicationException
    {
        private Device device;
        public DeviceException(Device device)
        {
            this.device = device;
        }

        public DeviceException(Device device, string message) : base(message)
        {
            this.device = device;
        }

        public DeviceException(Device device, string message, Exception innerException) : base(message, innerException)
        {
            this.device = device;
        }

        protected DeviceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }

        public override string Message => $"{this.device.Name}: {base.Message}";
    }
}
