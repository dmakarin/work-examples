﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intbus.Extensions
{
    public static class ByteArrayExtension
    {
        public static string ToStringHex(this IEnumerable<byte> bytes)
        {
            return bytes
                .Select(b => b.ToString("X2"))
                .Aggregate((n, p) => n + " " + p);
        }

        public static IEnumerable<byte> ReverseByByte(this IEnumerable<byte> bytes)
        {
            if (bytes.Count() % 2 != 0)
                throw new ApplicationException("Sequence count not even(2)");
            if (bytes.Count() <= 1)
                return bytes;

            var arr = bytes.ToArray();
            for (int i = 0; i < arr.Length - 1; i += 2)
            {
                var temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
            return arr;
        }

        public static IEnumerable<byte> ReverseByWord(this IEnumerable<byte> bytes)
        {
            if (bytes.Count() % 4 != 0)
                throw new ApplicationException("Sequence count not even(4)");
            if (bytes.Count() <= 2)
                return bytes;

            var arr = bytes.ToArray();
            for (int i = 0; i < arr.Length - 3; i += 4)
            {
                var temp1 = arr[i];
                var temp2 = arr[i + 1];
                arr[i] = arr[i + 2];
                arr[i + 1] = arr[i + 3];
                arr[i + 2] = temp1;
                arr[i + 3] = temp2;
            }
            return arr;
        }
    }
}
