﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.IO.Ports;
using Intbus.Helpers;

namespace Intbus.Tests
{
    //A1, A2 ... - обозначение требований из документации на Intbus
    [TestClass()]
    public class DeviceTests : IDisposable
    {
        //Для работы тестов нужно создать пару виртуальных портов, например, программой com0com
        private const string virtualPort1 = "COM31";
        private const string virtualPort2 = "COM32";

        public DeviceTests()
        {
            requestPort.Open();
            responsePort.Open();
            stream = requestPort.BaseStream;
            stream.ReadTimeout = 50;

            sensor = new Device(stream, Interface.OWI, 1, "датчик_А");
            sensor3 = new Device(stream, Interface.OWI, 1, "датчик_Ц");
            sensor2 = new Device(stream, Interface.OWI, 0x22, "датчик2_А");
            valve = new Device(stream, Interface.UART, 1, "клапан_Д");
            valve.AddSlave(sensor);
            valve.AddSlave(sensor2);
            sensor.AddSlave(sensor3);
            oModem = new Device(stream, null, 1, new List<Device>() { valve }, "ОхМодем_Е");
        }

        private SerialPort requestPort = new SerialPort(virtualPort1, 115200, Parity.None, 8, StopBits.One);
        private SerialPort responsePort = new SerialPort(virtualPort2, 115200, Parity.None, 8, StopBits.One);
        private static Stream stream;
        private static Device sensor;
        private static Device sensor2;
        private static Device sensor3;
        private static Device valve;
        private static Device oModem;

        [TestMethod()]
        public void GetFullPreambuleTest()
        {
            byte[] expected = new byte[] { 0x21, 0xA1, 0x01 };
            byte[] actual = sensor.GetFullPreambule().ToArray();

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void IncreseDepth()
        {
            Assert.AreEqual(2, sensor.Depth);
            Assert.AreEqual(2, sensor2.Depth);
            Assert.AreEqual(1, valve.Depth);
            Assert.AreEqual(0, oModem.Depth);
        }

        [TestMethod()]
        public void ExtendedAddress_A1()
        {
            byte[] expected = new byte[] { 0x21, 0xA1, 0x1F, 0x22 };
            byte[] actual = sensor2.GetFullPreambule().ToArray();

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void HoldingRead1Reg()
        {
            byte[] expected = new byte[] { 0x00, 0x01 };
            byte[] testResponse = new byte[] { 0x21, 0xA1, 0x01, 0x03, 0x02, 0x00, 0x01, 0x6E, 0xF7 };

            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse, 0, testResponse.Length);
            });
            Response response = sensor.ReadHoldingRegsAsync(0, 1).Result;

            byte[] actual = response.Buffer;

            Assert.IsFalse(response.HasError);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void HoldingRead2Regs()
        {
            byte[] expected = new byte[] { 0x01, 0x02, 0x03, 0x04 };
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0x01, 0x03, 0x04, 0x01, 0x02, 0x03, 0x04 };
            testResponse = CRC.ConcatCrc(testResponse);

            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.ToArray(), 0, testResponse.Count());
            });
            Response response = sensor.ReadHoldingRegsAsync(0, 2).Result;

            byte[] actual = response.Buffer;

            Assert.IsFalse(response.HasError);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ModbusError_A2()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0x01, 0x83, 0x01 };
            testResponse = CRC.ConcatCrc(testResponse);

            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.ToArray(), 0, testResponse.Count());
            });

            Response response = sensor.ReadHoldingRegsAsync(0, 1).Result;

            byte[] actual = response.Buffer;

            Assert.IsTrue(response.HasError);
            Assert.AreEqual("датчик_А", response.Device.Name);
            Assert.AreEqual(Error.MbIllegalFunction, response.Err);
        }

        [TestMethod()]
        public void SlaveNotResponse_A3()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA0, 0x01 };
            testResponse = CRC.ConcatCrc(testResponse);

            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.ToArray(), 0, testResponse.Count());
            });
            Response response = sensor.ReadHoldingRegsAsync(0, 1).Result;

            byte[] actual = response.Buffer;

            Assert.IsTrue(response.HasError);
            Assert.AreEqual("клапан_Д", response.Device.Name);
            Assert.AreEqual(Error.IbNotRespond, response.Err);
        }

        [TestMethod()]
        public void SlaveNotResponse_A4()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0x00, 0x01 };
            testResponse = CRC.ConcatCrc(testResponse);

            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.ToArray(), 0, testResponse.Count());
            });
            Response response = sensor.ReadHoldingRegsAsync(0, 1).Result;

            byte[] actual = response.Buffer;

            Assert.IsTrue(response.HasError);
            Assert.AreEqual("датчик_А", response.Device.Name);
            Assert.AreEqual(Error.IbNotRespond, response.Err);
        }

        [TestMethod()]
        public void SlaveHaveNotInterface_A5()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA0, 0xFF };
            testResponse = CRC.ConcatCrc(testResponse);

            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.ToArray(), 0, testResponse.Count());
            });
            Response response = sensor.ReadHoldingRegsAsync(0, 1).Result;

            byte[] actual = response.Buffer;

            Assert.IsTrue(response.HasError);
            Assert.AreEqual("ОхМодем_Е", response.Device.Name);
            Assert.AreEqual(Error.IbInterfaceNotSupport, response.Err);
        }

        [TestMethod()]
        public void SlaveHaveNotInterface_A6()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0x00, 0xFF };
            testResponse = CRC.ConcatCrc(testResponse);

            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.ToArray(), 0, testResponse.Count());
            });
            Response response = sensor.ReadHoldingRegsAsync(0, 1).Result;

            byte[] actual = response.Buffer;

            Assert.IsTrue(response.HasError);
            Assert.AreEqual("клапан_Д", response.Device.Name);
            Assert.AreEqual(Error.IbInterfaceNotSupport, response.Err);
        }

        [TestMethod()]
        public void SlaveBusy_A7()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0xFF, 0x05 };
            testResponse = CRC.ConcatCrc(testResponse);

            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.ToArray(), 0, testResponse.Count());
            });

            Response response = sensor.ReadHoldingRegsAsync(0, 1).Result;

            byte[] actual = response.Buffer;

            Assert.IsFalse(response.HasError);
            Assert.AreEqual("клапан_Д", response.Device.Name);
            Assert.AreEqual(Error.IbCommandExecuting, response.Err);
        }

        [TestMethod()]
        public void SlaveInterfaceBusy_A8()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0xFF, 0x06 };
            testResponse = CRC.ConcatCrc(testResponse);

            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.ToArray(), 0, testResponse.Count());
            });

            Response response = sensor.ReadHoldingRegsAsync(0, 1).Result;

            byte[] actual = response.Buffer;

            Assert.IsFalse(response.HasError);
            Assert.AreEqual("клапан_Д", response.Device.Name);
            Assert.AreEqual(Error.IbInterfaceBusy, response.Err);
        }

        [TestMethod()]
        public void TransmitError_A9()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0xFF, 0x0A };
            testResponse = CRC.ConcatCrc(testResponse);

            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.ToArray(), 0, testResponse.Count());
            });

            Response response = sensor.ReadHoldingRegsAsync(0, 1).Result;

            byte[] actual = response.Buffer;

            Assert.IsTrue(response.HasError);
            Assert.AreEqual("клапан_Д", response.Device.Name);
            Assert.AreEqual(Error.IbTransmitError, response.Err);
        }

        [TestMethod()]
        public void WriteCoilRegsAsyncTest()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0x01, 0x00, 0x05, 0xFF, 0x00, 0x00 };
            testResponse = CRC.ConcatCrc(testResponse);

            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.ToArray(), 0, testResponse.Count());
            });
            Response response = sensor.WriteCoilRegsAsync(1, true).Result;

            byte[] actual = response.Buffer;

            Assert.IsFalse(response.HasError);
        }

        // Тесты на отработку "рваного" ответа
        [TestMethod()]
        public void WriteDoubleResponseWithDelay()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0x01, 0x00, 0x05, 0xFF, 0x00, 0x00 };
            testResponse = CRC.ConcatCrc(testResponse);
            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.Take(4).ToArray(), 0, 4);
                await Task.Delay(40);
                responsePort.Write(testResponse.Skip(4).ToArray(), 0, testResponse.Count() - 4);
            });
            Response response = sensor.WriteCoilRegsAsync(1, true).Result;

            byte[] actual = response.Buffer;

            Assert.IsFalse(response.HasError);
        }

        [TestMethod()]
        public void CRCError()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0x01, 0x00, 0x03, 0xFF, 0x00, 0x00, 0x11, 0x11 };
            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.Take(4).ToArray(), 0, 4);
                await Task.Delay(40);
                responsePort.Write(testResponse.Skip(4).ToArray(), 0, testResponse.Count() - 4);
            });
            Response response = sensor.WriteCoilRegsAsync(1, true).Result;

            byte[] actual = response.Buffer;

            Assert.AreEqual(response.Err, Error.CRC);
        }

        [TestMethod()]
        public void ResponseOverflowError()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11 };
            Task.Run(() =>
            {
                responsePort.Write(testResponse.ToArray(), 0, testResponse.Count());
            });
            Response response = sensor.WriteCoilRegsAsync(1, true).Result;

            byte[] actual = response.Buffer;

            Assert.AreEqual(response.Err, Error.ResponseOverflow);
        }

        [TestMethod()]
        public void WakeUpBytes()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0x01, 0x00, 0x03, 0xFF, 0x00, 0x00 };
            testResponse = CRC.ConcatCrc(testResponse);
            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.Take(4).ToArray(), 0, 4);
                await Task.Delay(40);
                responsePort.Write(testResponse.Skip(4).ToArray(), 0, testResponse.Count() - 4);
            });
            oModem.WakeUpByteAmount = 2;
            sensor.BytesSended += Sensor_BytesSended;
            Response response = sensor.WriteCoilRegsAsync(1, true).Result;

            sensor.BytesSended -= Sensor_BytesSended;

            byte[] actual = response.Buffer;
        }

        [TestMethod()]
        public void WriteHoldingMultiplyAsyncTest()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA1, 0x01, 0x10, 0x00, 0x05, 0x00, 0x02};
            testResponse = CRC.ConcatCrc(testResponse);
            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.Take(4).ToArray(), 0, 4);
                await Task.Delay(40);
                responsePort.Write(testResponse.Skip(4).ToArray(), 0, testResponse.Count() - 4);
            });
            Response response = sensor.WriteHoldingMultiplyAsync(5, new UInt16[] { 0x0001, 0x0002}).Result;

            byte[] actual = response.Buffer;

            Assert.IsFalse(response.HasError);
        }

        [TestMethod()]
        public void ReadInputWithError()
        {
            IEnumerable<byte> testResponse = new byte[] { 0x21, 0xA0, 0x01 };
            testResponse = CRC.ConcatCrc(testResponse);
            Task.Run(async () =>
            {
                await Task.Delay(5);
                responsePort.Write(testResponse.Take(4).ToArray(), 0, 4);
                await Task.Delay(40);
                responsePort.Write(testResponse.Skip(4).ToArray(), 0, testResponse.Count() - 4);
            });
            Response response = sensor3.ReadInputRegsAsync(1, 1).Result;

            byte[] actual = response.Buffer;

            Assert.IsTrue(response.HasError);
            Assert.AreEqual("клапан_Д", response.Device.Name);
            Assert.AreEqual(Error.IbNotRespond, response.Err);
        }

        private void Sensor_BytesSended(IEnumerable<byte> bytes)
        {
            CollectionAssert.AreEqual(new byte[] { 0xFF, 0xFF, 0x21, 0xA1, 0x01, 0x05, 0x00, 0x01, 0xFF, 0x00, 0xEE, 0xF4 }, bytes.ToArray());
        }
        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                    this.requestPort.Dispose();
                    this.responsePort.Dispose();
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        // ~DeviceTests()
        // {
        //   // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
        //   Dispose(false);
        // }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            // GC.SuppressFinalize(this);
        }
        #endregion


    }
}