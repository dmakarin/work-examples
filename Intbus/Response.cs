﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intbus
{
    public class Response
    {
        public static Dictionary<byte, Error> ErrorMbCodeDictionary { get; private set; }
            = new Dictionary<byte, Error>
        {
            {0x01, Error.MbIllegalFunction},
            {0x02, Error.MbIllegalDataAddress},
            {0x03, Error.MbIllegalDataValue},
            {0x04, Error.MbSlaveDeviceFailure},
            {0x05, Error.MbAcknowledge},
            {0x06, Error.MbSlaveDeviceBusy},
            {0x07, Error.MbNegativeAcknowledge},
            {0x08, Error.MbMemoryParityError},
            {0x10, Error.MbGatewayPathUnavailable},
            {0x11, Error.MbGatewayTargetDeviceFailedToResponde},
        };

        public static IReadOnlyDictionary<byte, Error> ErrorIbCodeDictionary { get; private set; }
            = new Dictionary<byte, Error>
        {
            {0x05, Error.IbCommandExecuting },
            {0x06, Error.IbInterfaceBusy },
            {0x0A, Error.IbTransmitError }
        };
        public TimeSpan ExpectingTime { get; }

        public Error? Err { get; }

        public bool HasError => Err != null && Err != Error.IbInterfaceBusy && Err != Error.IbCommandExecuting;

        public byte[] Buffer { get; private set; }

        public Device Device { get; }
        public Response(Device device, IEnumerable<byte> buffer, DateTime beginTime)
        {
            this.Device = device ?? throw new ArgumentException("Device shouldn't be null", nameof(device));
            this.Buffer = buffer.ToArray();
            this.ExpectingTime = DateTime.Now - beginTime;
        }

        public Response(Device device, IEnumerable<byte> buffer, DateTime time, Error error) : this(device, buffer, time)
        {
            this.Err = error;
        }
    }

    public enum Error
    {
        MbIllegalFunction,
        MbIllegalDataAddress,
        MbIllegalDataValue,
        MbSlaveDeviceFailure,
        MbAcknowledge,
        MbSlaveDeviceBusy,
        MbNegativeAcknowledge,
        MbMemoryParityError,
        MbGatewayPathUnavailable,
        MbGatewayTargetDeviceFailedToResponde,
        IbCommandExecuting,
        IbInterfaceBusy,
        IbTransmitError,
        IbInterfaceNotSupport,
        IbNotRespond,
        ShortResponse,
        UnexpectedFunction,
        Timeout,
        CRC,
        ResponseOverflow
    }
}
